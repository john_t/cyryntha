#define LENGTH(X)               (sizeof X / sizeof X[0])

int ledPin = LED_BUILTIN;

typedef struct Button {
    char * name;
    short pin;
    int value;
} Button;

#include "config.h"

void setup() {
  pinMode(ledPin, OUTPUT);

  // Initialise every pin
  for (int i = 0; i < LENGTH(buttons); i++) {
      Button button = buttons[i];
      pinMode(button.pin, INPUT);
  }
  Serial.begin(9600);
}

void loop() {
  for (int i = 0; i < LENGTH(buttons); i++) {
      Button * button = buttons + i;
      checkButton(&button->value, button->pin, button->name);
  }
}

/**
    Monitors a button and if it changes it will
    change its value and print to the serial
  */
void checkButton(
  int * val,
  int buttonId,
  char * name
) {
  bool readVal = digitalRead(buttonId);
  if (readVal != *val) {
    
    /*
       Prints to the serial the instruction in the form of:
      
       setPin <n> <val>
     */
    Serial.print("setPin ");
    Serial.print(name);
    Serial.print(' ');
    Serial.println(readVal);
    
    /* Updates the value */
    *val = readVal;
  }
}
