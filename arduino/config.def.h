static Button buttons[] = {
    /* key		pin			value	*/
    {"a",		1,			0		},
    {"b",		2,			0		},
    {"y",		3,			0		},
    {"x",		4,			0		},

    {"dpad-d",	5,			0		},
    {"dpad-r",	6,			0		},
    {"dpad-l",	7,			0		},
    {"dpad-u",	8,			0		},
};
