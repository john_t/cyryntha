use std::io;

use bytes::BytesMut;
use tokio_util::codec::Decoder;

pub struct LineCodec;

impl Decoder for LineCodec {
    type Item = String;
    type Error = io::Error;

    fn decode(
        &mut self,
        src: &mut BytesMut,
    ) -> Result<Option<Self::Item>, Self::Error> {
        let newline = src.as_ref().iter().position(|b| *b == b'\n');
        if let Some(n) = newline {
            let line = src.split_to(n + 1);
            return match std::str::from_utf8(line.as_ref()) {
                Ok(s) => Ok(Some(s.to_string())),
                Err(_) => {
                    Err(io::Error::new(io::ErrorKind::Other, "Invalid String"))
                }
            };
        }
        Ok(None)
    }
}
