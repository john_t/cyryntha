#![warn(clippy::pedantic)]

pub mod gamepad;
pub use gamepad::GamePad;

pub mod instruction;
pub use cyryntha::Config;
pub use instruction::Instruction;

pub mod line_codec;

use std::fs;

use futures::StreamExt;
use line_codec::LineCodec;
use tokio_serial::SerialPortBuilderExt;
use tokio_util::codec::Decoder;

pub const BAUD_RATE: u32 = 9600;
pub const PATH: &str = "/dev/ttyACM0";

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Determine where out out filesystem the config should be
    let mut config = dirs::config_dir().expect("Cannot get home directory...");
    config.push("cyryntha/");

    if !config.exists() {
        fs::create_dir_all(&config)?;
    }

    config.push("config.yaml");

    // If it doesn't exist we will copy the default.
    if !config.exists() {
        fs::write(&config, include_bytes!("../example_config.yaml"))?;
    }

    // Read our config
    let config: Config = serde_yaml::from_str(&fs::read_to_string(config)?)?;

    let todo = config.gamepads.into_iter().map(run_gamepad);
    futures::future::try_join_all(todo).await?;

    Ok(())
}

async fn run_gamepad(config: cyryntha::config::GamePad) -> std::io::Result<()> {
    let mut port = tokio_serial::new(config.port.clone(), config.baud_rate)
        .open_native_async()?;

    #[cfg(unix)]
    port.set_exclusive(false)
        .expect("Unable to set serial port exclusive to false");

    let mut reader = LineCodec.framed(port);

    println!("Registered gamepad {}", config.name);
    let mut gamepad = GamePad::new(config)?;

    while let Some(line_result) = reader.next().await {
        let line = line_result?;
        let parsed = line.parse();
        match parsed {
            Ok(o) => {
                let parsed = o;
                println!("{:?}", parsed);
                gamepad.read_instruction(&parsed)?;
            }
            Err(_) => eprintln!("Unknown/invalid instruction!"),
        }
    }

    Ok(())
}
