use std::borrow::Cow;
use std::str::FromStr;

#[derive(Debug)]
pub struct ParseError;

#[derive(Debug, Clone)]
pub enum Instruction<'a> {
    SetPin(SetPin<'a>),
}

impl<'a> FromStr for Instruction<'a> {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Instruction::SetPin(s.trim().parse()?))
    }
}

#[derive(Debug, Clone)]
pub struct SetPin<'a> {
    pub pin: Cow<'a, str>,
    pub value: u8,
}

impl<'a> FromStr for SetPin<'a> {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut split = s.split(' ');
        if split.next() == Some("setPin") {
            Ok(Self {
                pin: Cow::Owned(split.next().ok_or(ParseError)?.to_string()),
                value: split
                    .next()
                    .ok_or(ParseError)?
                    .parse()
                    .map_err(|_| ParseError)?,
            })
        } else {
            Err(ParseError)
        }
    }
}
