use std::io;
use std::time::{SystemTime, UNIX_EPOCH};

use cyryntha::config::{GamePad as Config, Pin};
use evdev::{
    enums::{BusType, EventCode, EventType, EV_ABS, EV_KEY, EV_SYN},
    AbsInfo, DeviceWrapper, InputEvent, TimeVal, UInputDevice, UninitDevice,
};

use crate::Instruction;

pub struct GamePad {
    pub device: UInputDevice,
    pub config: Config,
}

impl GamePad {
    /// Creates a new gamepad
    ///
    /// # Errors
    ///
    /// Errors if it cannot talk to evdev
    pub fn new(config: Config) -> io::Result<Self> {
        Ok(GamePad {
            device: {
                let u = UninitDevice::new().ok_or_else(|| {
                    io::Error::new(io::ErrorKind::NotConnected, "Ah shoot")
                })?;
                u.set_name("Cyryntha");
                u.set_bustype(BusType::BUS_USB as u16);
                u.set_vendor_id(0x1234);
                u.set_product_id(0x5678);

                u.enable_event_type(&EventType::EV_KEY)?;
                for key in &config.available.keys {
                    u.enable_event_code(&EventCode::EV_KEY(*key), None)?;
                }

                u.enable_event_type(&EventType::EV_ABS)?;
                for axis in &config.available.axes {
                    let absinfo = AbsInfo {
                        value: axis.value,
                        minimum: axis.minimum,
                        maximum: axis.maximum,
                        fuzz: 0,
                        flat: 0,
                        resolution: 0,
                    };
                    u.enable_event_code(
                        &EventCode::EV_ABS(axis.axis),
                        Some(&absinfo),
                    )?;
                }

                UInputDevice::create_from_device(&u)?
            },
            config,
        })
    }

    /// Reads an instruction,
    ///
    /// # Errors
    ///
    /// Errors if it cannot talk to evdev
    pub fn read_instruction(
        &mut self,
        instruction: &Instruction<'_>,
    ) -> io::Result<()> {
        match instruction {
            Instruction::SetPin(ins) => {
                let axis_override = if ins.value == 0 { Some(0) } else { None };
                if let Some(pin) =
                    self.config.mappings.pins.get(ins.pin.as_ref())
                {
                    match pin {
                        Pin::Digital(digital) => {
                            self.event_key(digital.key, ins.value > 0)?;
                        }
                        Pin::Axis(axis) => {
                            self.event_axis(
                                axis.axis,
                                axis_override.unwrap_or(axis.value),
                            )?;
                        }
                    }
                } else {
                    return Err(io::Error::new(
                        io::ErrorKind::InvalidInput,
                        format!("Unknown pin: {}", ins.pin),
                    ));
                }
                self.event_report()?;
            }
        }
        Ok(())
    }

    fn event_key(&self, key: EV_KEY, up: bool) -> io::Result<()> {
        let event = event_key(key, up);
        self.device.write_event(&event)?;
        self.device.write_event(&event)?;
        Ok(())
    }

    fn event_axis(&self, axis: EV_ABS, val: i32) -> io::Result<()> {
        let event = event_axis(axis, val);
        self.device.write_event(&event)?;
        Ok(())
    }

    fn event_report(&self) -> io::Result<()> {
        let event = event_report();
        self.device.write_event(&event)?;
        Ok(())
    }
}

fn event_report() -> InputEvent {
    InputEvent::new(
        &event_current_time(),
        &EventCode::EV_SYN(EV_SYN::SYN_REPORT),
        0,
    )
}

fn event_axis(axis: EV_ABS, val: i32) -> InputEvent {
    InputEvent::new(&event_current_time(), &EventCode::EV_ABS(axis), val)
}

fn event_key(key: EV_KEY, up: bool) -> InputEvent {
    InputEvent::new(
        &event_current_time(),
        &EventCode::EV_KEY(key),
        i32::from(up),
    )
}

fn event_current_time() -> TimeVal {
    let now = SystemTime::now();
    let since = now.duration_since(UNIX_EPOCH).unwrap();
    TimeVal::new(
        i64::try_from(since.as_secs()).unwrap(),
        i64::from(since.subsec_micros()),
    )
}
