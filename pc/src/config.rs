use std::collections::HashMap;

use evdev::enums::{EV_ABS, EV_KEY};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Config {
    pub gamepads: Vec<GamePad>,
}

#[derive(Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct GamePad {
    pub port: String,
    pub name: String,
    pub baud_rate: u32,
    pub available: Available,
    pub mappings: Mappings,
}

#[derive(Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Available {
    pub keys: Vec<EV_KEY>,
    pub axes: Vec<Axis>,
}

#[derive(Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Axis {
    pub axis: EV_ABS,
    pub minimum: i32,
    pub maximum: i32,
    pub value: i32,
}

#[derive(Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Mappings {
    pub pins: HashMap<String, Pin>,
}

#[derive(Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
#[serde(untagged)]
pub enum Pin {
    Digital(DigitalPin),
    Axis(AxisPin),
}

#[derive(Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DigitalPin {
    pub key: EV_KEY,
}

#[derive(Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct AxisPin {
    pub axis: EV_ABS,
    pub value: i32,
}
