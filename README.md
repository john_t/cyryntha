# Cyryntha

Cyryntha is a small utility program for linux that allows you to use an arduino
as a gamepad.

Look in the [arduino folder](/arduino) to see the code (which you may need to
modify to fit your pin arrangement) for the arduino.

The [pc directory](/pc) contains a desktop application which can be configured
to read from many arduinos.
